package com.example.BankSpringBoot;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class PersonBank {
    private Integer balance;
    private Integer age;

    public PersonBank() {
        this.balance=10000;

    }
    public void accept(){
    Scanner scanner=new Scanner(System.in);
    System.out.println("Enter person age:");
    this.age=scanner.nextInt();
}
    public PersonBank(Integer balance, Integer age) {
        this.balance = balance;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "balance=" + balance +
                ", age=" + age +
                '}';
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
